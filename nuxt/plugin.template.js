import Vue from 'vue'
import ClickOutside from 'vue-click-outside'
import Resource from '@provide-smart/core/src/models/Resource'
import { DatatableModule } from '@provide-smart/core/src/components/datatable'
import { CardModule } from '@provide-smart/core/src/components/card'
import { BadgeModule } from '@provide-smart/core/src/components/badge'
import { ButtonModule } from '@provide-smart/core/src/components/button'
import { DropdownModule } from '@provide-smart/core/src/components/dropdown'
import { ListGroupModule } from '@provide-smart/core/src/components/list-group'
import { TabsModule } from '@provide-smart/core/src/components/tabs'
import { ModalModule } from '@provide-smart/core/src/components/modal'
import Datatable from '@provide-smart/core/src/models/Datatable'
import { FormModule } from '@provide-smart/core/src/components/form'
import { PaginationModule } from '@provide-smart/core/src/components/pagination'
import { HeaderModule } from '@provide-smart/core/src/components/header'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import { SkeletonModule } from '@provide-smart/core/src/components/skeleton'
import Activity from '@provide-smart/core/src/models/Activity'
import User from '@provide-smart/core/src/models/User'
import { SpinnerModule } from '@provide-smart/core/src/components/spinner'
import { EventBus } from '@provide-smart/core/src/utils/EventBus'
import { DirectiveModule } from '@provide-smart/core/src/directives'
import { TooltipModule } from '@provide-smart/core/src/components/tooltip'
import { ActionModule } from '@provide-smart/core/src/components/actions'
import { CommonModule } from '@provide-smart/core/src/components/common'
import ResourceTranslator from '~/modules/@provide-smart/core/src/utils/ResourceHelper'
import dayjs from "dayjs";

export default ({ store, app, axios }, inject) => {
    store.$db().register(Resource)
    store.$db().register(Activity)
    store.$db().register(User)
    store.$db().register(Datatable)

    Vue.component('fa', FontAwesomeIcon)
    Vue.component('fa-layers', FontAwesomeLayers)
    Vue.component('fa-layers-text', FontAwesomeLayersText)

    // TODO: Fix this bug someday -> it loads components from the nuxt.config.js but components throw `_init of undefined` errors
    //  -> uses serialize function to resolve options from nuxt module

    // Vue.use(VueTailwind, options.componentStyling)

    Vue.use(ActionModule, { store, app, inject })
    Vue.use(BadgeModule)
    Vue.use(ButtonModule)
    Vue.use(CardModule)
    Vue.use(CommonModule)
    Vue.use(DatatableModule, { store })
    Vue.use(DropdownModule)
    Vue.use(FormModule)
    Vue.use(HeaderModule)
    Vue.use(ListGroupModule)
    Vue.use(ModalModule, { store, app, inject })
    Vue.use(PaginationModule)
    Vue.use(TabsModule)
    Vue.use(SkeletonModule)
    Vue.use(SpinnerModule)
    Vue.use(TooltipModule)
    Vue.use(DirectiveModule)

    Vue.directive('ClickOutside', ClickOutside)

    Vue.filter('formatDate', function(value) {
        if (!value) return null;
        return dayjs(value).format('DD-MM-YYYY HH:mm:ss');
    });

    inject('eventBus', EventBus)
    inject('resourceHelper', new ResourceTranslator(app))
}
