import * as path from 'path'

export default function ProvideSmartCoreModule(options) {
    this.nuxt.hook('build:before', () => {
        // this.addModule('@nuxtjs/i18n')

        this.addPlugin({
            src: path.resolve(__dirname, 'plugin.template.js'),
            options,
        })

        this.addPlugin({
            src: path.resolve(__dirname, '../src/services/ToastService.ts'),
            options,
        })

        this.addPlugin({
            src: path.resolve(__dirname, '../src/services/ResourceService.ts'),
            options,
        })
    })
}
