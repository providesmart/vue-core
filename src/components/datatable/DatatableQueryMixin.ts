import Vue from 'vue'
import { Item } from '@vuex-orm/core'
import { Prop, Watch } from 'vue-property-decorator'
import { Component } from 'nuxt-property-decorator'
import Datatable from '../../models/Datatable'
import Query from '../../utils/Query'

@Component
export default class DatatableQueryMixin extends Vue {
    public datatableId: string | null = null

    @Prop({ type: [String] })
    datatableUuid!: string

    get datatableState(): Item<Datatable> | null {
        return this.datatableId ? Datatable.query().where('uuid', this.datatableId).first() : null
    }

    get queryState(): Query | undefined {
        return this.datatableState?.query
    }

    get selectionState(): {} | undefined {
        return this.datatableState?.selection
    }

    @Watch('datatableUuid', { immediate: true })
    onDatatableUuidChanged(val: string) {
        this.datatableId = val
    }

    public async initDatatable(data: any = {}): Promise<Item<Datatable> | null> {
        await Datatable.insert({
            data: [data],
        })

        const datatable: Item<Datatable> = await Datatable.query().last()

        this.datatableId = datatable ? datatable.uuid : null

        return datatable
    }

    public updateQuery(query: Query, pageReset = true) {
        this.updateDatatableState({
            ...this.datatableState,
            query: { ...this.datatableState?.query, ...query, page: pageReset ? 1 : query.page },
        })

        this.updateSelection([])
    }

    public updateSelection(selection: any) {
        this.updateDatatableState({
            ...this.datatableState,
            selection,
        })
    }

    protected updateDatatableState(state: any) {
        if (this.datatableState?.$id) {
            Datatable.update({
                where: this.datatableState.$id,
                data: state,
            })
        }
    }
}
