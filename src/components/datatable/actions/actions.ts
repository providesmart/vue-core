export interface ActionInterface {
    name: string
    callback: Function
    icon?: string | string[]
    cancelButtonText?: string
    class?: string
    component?: string
    confirmButtonText?: string
    confirmText?: string
    destructive?: boolean
    fields?: any[]
    uriKey?: string
    withoutConfirmation?: boolean
}
