import { Component, Prop, Vue } from 'vue-property-decorator'
import { ColumnModel } from './../columns/model'

@Component
export default class IndexFieldMixin extends Vue {

    @Prop({
        type: [Object], default: () => {
        },
    })
    column!: ColumnModel

    @Prop({
        default: () => {
        },
    })
    item!: any

    get value() {
        return this.item[this.column.name]
    }

}
