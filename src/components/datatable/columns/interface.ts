import { ClipMode } from './enum'

export interface ColumnInterface {
    /**
     * The name of the column; typically a snake or camel case name
     */
    name: string

    /**
     * The label that is displayed as header of the column;
     * if blank -> the name will be displayed
     */
    label?: string | null

    /**
     * Defines if the column should be visible
     */
    active?: boolean

    /**
     * Column is sticky in the beginning of the table
     */
    sticky?: boolean

    /**
     * Whether the column is filterable
     */
    filterable?: boolean

    /**
     * Whether the column is sortable
     */
    sortable?: boolean

    /**
     * Sort direction, null if disabled
     */
    sortDirection?: 'asc' | 'desc' | undefined

    /**
     * The order of the column in the set of columns
     */
    index?: number

    /**
     * Defines if the column should be fixed on the left side of the table
     */
    fixed?: boolean

    /**
     * The width of the column;
     * if blank -> width is calculated based on the content
     */
    width?: string | number | null

    /**
     * Text alignment (left, right, center)
     */
    textAlign?: string

    /**
     * Defines the cell's overflow method
     */
    clipMode?: ClipMode

    /**
     * Other options
     */
    options?: any

    /**
     * Does the column represent a relation?
     */
    isRelation?: boolean

    /**
     * If the relationName differs from the attribute name
     */
    relationName?: string
}
