import { ColumnInterface } from './interface'
import { ClipMode } from './enum'
import Field from '~/modules/@provide-smart/core/src/models/fields/Field'

export class ColumnModel extends Field implements ColumnInterface {
    public name: string                  = ''
    public label: string | null          = null
    public sticky: boolean               = false
    public width: string | number | null = null
    public active: boolean               = true
    public clipMode: ClipMode            = ClipMode.CLIP
    public filterable: boolean           = false
    public sortable: boolean             = false
    public fixed: boolean                = false
    public isRelation: boolean           = false
    public relationName!: string
    public options!: any
    public lines!: any

    public constructor(data: Partial<ColumnModel>) {
        super(data)
        Object.assign(this, data)
    }

    public getLabel() {
        return this.label || this.name
    }

    public getColumnWidthInPixels() {
        return this.width
            ? this.width + 'px'
            : '150px'
    }
}
