export enum ClipMode {
    CLIP     = 'clip',
    ELLIPSIS = 'ellipsis',
    NONE     = 'none'
}
