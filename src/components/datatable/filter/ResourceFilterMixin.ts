import { Prop, Watch } from 'vue-property-decorator'
import { Component } from 'nuxt-property-decorator'
import QueryMixin from '../DatatableQueryMixin'
import FilterInterface from './interface'

@Component
export default class ResourceFilterMixin extends QueryMixin {
    public filterValue: any = ''

    public skipChangeDetection = false

    @Prop({
        type: [Object],
        required: false,
    })
    filter!: FilterInterface

    get activeFilters() {
        return this.datatableState?.query.filters || {}
    }

    get currentFilterValue() {
        return this.activeFilters[this.filterName()]
    }

    @Watch('currentFilterValue')
    onCurrentFilterValueChanged(value: any) {
        this.onCurrentFilterValueChange(value)
    }

    public getFilterValue(filterName: string) {
        return this.activeFilters[filterName] || null
    }

    public filterName() {
        return this.filter.attribute
    }

    public onCurrentFilterValueChange(value: any) {
        console.log('CURRENT FILTER VALUE CHANGED', value)
    }
}
