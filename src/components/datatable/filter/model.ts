import FilterInterface from './interface'

export default class FilterModel implements FilterInterface {

    public component: string   = 'text-filter';
    public name: string        = '';
    public attribute: string   = '';
    public searchable: boolean = false;
    public sticky: boolean     = false;
    public api: boolean        = false
    public options?: []        = []
    public optionLabelKey?: string
    public optionValueKeyKey?: string
    public relatedResourceName?: string

    public constructor(data: Partial<FilterModel>) {
        Object.assign(this, data);
    }

}
