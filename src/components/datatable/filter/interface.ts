export default interface FilterInterface {
    name: string;
    attribute: string;
    id?: string;
    component: string;
    searchable: boolean;
    sticky: boolean;
    relatedResourceName?: string;
    operators?: [];
    options?: [];
    optionLabelKey?: string,
    optionValueKey?: string,
    api?: boolean;
}
