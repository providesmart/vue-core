import { NAME_DATATABLE } from '../../constants/components'
import Datatable from './Datatable.vue'

export const DatatableModule: any = {
    install: (Vue: any, options: any) => {
        if (!options || !options.store) {
            throw new Error('Please initialise plugin with a Vuex store.')
        }

        Vue.component(NAME_DATATABLE, Datatable)

        Vue.component('IndexCurrencyField', () => import('@provide-smart/core/src/components/datatable/fields/IndexCurrencyField.vue'))
        Vue.component('IndexFileField', () => import('@provide-smart/core/src/components/datatable/fields/IndexFileField.vue'))
        Vue.component('IndexBadgeField', () => import('@provide-smart/core/src/components/datatable/fields/IndexBadgeField.vue'))
        Vue.component('IndexBooleanField', () => import('@provide-smart/core/src/components/datatable/fields/IndexBooleanField.vue'))
        Vue.component('IndexBelongsToField', () => import('@provide-smart/core/src/components/datatable/fields/IndexBelongsToField.vue'))
        Vue.component(
            'IndexBelongsToManyField',
            () => import('@provide-smart/core/src/components/datatable/fields/IndexBelongsToManyField.vue'),
        )
        Vue.component('IndexStackField', () => import('@provide-smart/core/src/components/datatable/fields/IndexStackField.vue'))
    },
}
