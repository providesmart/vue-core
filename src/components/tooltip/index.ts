import Tooltip from '@provide-smart/core/src/components/tooltip/Tooltip.vue'

export const TooltipModule: any = {
    install: (Vue: any) => {
        Vue.component('PsTooltip', Tooltip)
    },
}
