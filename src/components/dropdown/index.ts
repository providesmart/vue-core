import { NAME_DROPDOWN, NAME_DROPDOWN_SELECTABLE } from '../../constants/components';
import Dropdown from './Dropdown.vue';
import SelectableDropdown from './SelectableDropdown.vue';

export const DropdownModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_DROPDOWN, Dropdown);
        Vue.component(NAME_DROPDOWN_SELECTABLE, SelectableDropdown);
    }
}
