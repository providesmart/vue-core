import { Component } from 'nuxt-property-decorator'
import Vue from 'vue'
import { Prop, Watch } from 'vue-property-decorator'

@Component
export default class DropdownMixin extends Vue {
    public dropdownPosition = 'left'
    public verticalDropdownPosition = 'bottom'

    @Prop({ type: [String], default: 'left' })
    position!: 'left' | 'center' | 'right' | 'top left' | 'top right' | 'top center'

    @Prop({ type: [String], default: 'bottom' })
    verticalPosition!: string

    @Prop({ type: [Boolean], default: false })
    arrow!: boolean

    @Prop({ type: [String], default: 'primary' })
    buttonVariant!: string

    @Prop({ type: [String], default: 'base' })
    buttonSize!: string

    @Prop({ type: [String], default: 'w-80' })
    width!: string

    @Prop({ type: Boolean, default: false })
    fullWidth!: boolean

    @Prop({ type: Boolean, default: false })
    disabled!: boolean

    @Watch('position')
    onPositionChanged() {
        this.dropdownPosition = this.position
    }

    @Watch('verticalPosition')
    onVerticalPositionChanged() {
        this.verticalDropdownPosition = this.verticalPosition
    }
}
