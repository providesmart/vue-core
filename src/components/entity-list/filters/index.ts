import RangeFilter from '~/modules/@provide-smart/core/src/components/entity-list/filters/RangeFilter.vue';
import SelectFilter from '~/modules/@provide-smart/core/src/components/entity-list/filters/SelectFilter.vue';
import DateRangeFilter from '~/modules/@provide-smart/core/src/components/entity-list/filters/DateRangeFilter.vue';
import BooleanFilter from '~/modules/@provide-smart/core/src/components/entity-list/filters/BooleanFilter.vue';
import TextFilter from '~/modules/@provide-smart/core/src/components/entity-list/filters/TextFilter.vue';

export const Filters = {
    'range-filter': RangeFilter,
    'select-filter': SelectFilter,
    'date-range-filter': DateRangeFilter,
    'boolean-filter': BooleanFilter,
    'text-filter': TextFilter,
}
