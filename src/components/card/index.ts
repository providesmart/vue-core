import { NAME_CARD, NAME_CARD_BODY } from '../../constants/components';
import Card from './Card.vue';
import CardBody from './CardBody.vue';

export const CardModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_CARD, Card);
        Vue.component(NAME_CARD_BODY, CardBody);
    }
}
