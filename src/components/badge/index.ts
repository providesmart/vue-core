import { NAME_BADGE } from '../../constants/components';
import Badge from './Badge.vue';

export const BadgeModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_BADGE, Badge);
    }
}
