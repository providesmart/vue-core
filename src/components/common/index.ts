import Avatar from './avatar/Avatar.vue'

export const CommonModule: any = {
    install: (Vue: any) => {
        Vue.component('PsAvatar', Avatar)
    },
}
