import { ModalAction } from '~/modules/@provide-smart/core/src/components/modal/Modal'

export default interface ModalableComponent {
    /**
     * @return true if the modal can be accepted and dismissed
     */
    isValidForAccept(): Promise<boolean> | boolean

    getData(): any | null

    /**
     * Notifies the component that it is dismissed. Use this to cleanup or cancel running tasks
     */
    notifyDismissed(action?: ModalAction): Promise<void>

    /**
     * Notifies the component that it is accepted.
     */
    notifyAccepted(action?: ModalAction): Promise<void>

    notifyAcceptErrors?(payload: any): void
}
