import Vue from 'vue'
import { Component } from 'nuxt-property-decorator'
import { Prop, Ref } from 'vue-property-decorator'
import ModalableComponent from './ModalableComponent'
import { ModalAction, ModalActionType, ModalProps, ModalResult, ModalType } from './Modal'

@Component
export default class BaseModalComponent extends Vue {
    @Prop() readonly modal!: ModalProps
    @Prop(Number) readonly zIndex!: number
    @Prop(Number) readonly translate!: number

    @Ref('modalComponent') readonly modalComponent?: ModalableComponent

    public loading = false

    // noinspection JSUnusedLocalSymbols
    public visible = true // changing this will trigger the animation

    // Template helpers
    // noinspection JSUnusedLocalSymbols
    public actionTypeDismiss: ModalAction = { type: ModalActionType.DISMISS }
    // noinspection JSUnusedLocalSymbols
    public actionTypeAccept: ModalAction  = { type: ModalActionType.ACCEPT }

    protected async clickHandler(action: ModalAction) {
        const isDismissConfirmAction = action.tag === 'confirm-dismiss'
        if (this.shouldRequestDismissConfirmation(action) && !isDismissConfirmAction) {
            this.$modalService.open({
                title: 'Are you sure?',
                message: 'The entered data will be deleted',
                variant: 'danger',
                type: ModalType.FLOATING,
                shouldConfirmDismiss: false,
                onAccept: () => {
                    // Set the flag and recall the handler
                    this.modal.isDismissConfirmed = true
                    this.clickHandler(action)
                },
                actions: [
                    { type: ModalActionType.DISMISS, name: 'Cancel', variant: 'link' },
                    { type: ModalActionType.ACCEPT, name: 'Dismiss', variant: 'danger', tag: 'confirm-dismiss' },
                ],
            })
            return
        }

        let result: boolean = false
        switch (action.type) {
        case ModalActionType.ACCEPT:
            result = await this.handleAccept(action)
            break
        case ModalActionType.DISMISS:
            result = await this.handleDismiss(action)
            break
        }

        if (result) {
            const result = this.buildResult(action)
            setTimeout(async () => {
                try {
                    await this.$modalService.dismiss(this.modal, result)
                    this.visible = false
                } catch (e) {
                    if (this.modalComponent?.notifyAcceptErrors) {
                        this.modalComponent?.notifyAcceptErrors(e)
                    }
                }

                this.loading = false
            }, 300)
        }
    }

    private async handleAccept(action: ModalAction): Promise<boolean> {
        this.loading = true

        const validForAccept = await Promise.resolve<boolean>(this.modalComponent?.isValidForAccept() ?? true).catch(() => {
            return true
        })

        if (validForAccept) {
            await this.modalComponent?.notifyAccepted(action)
            return Promise.resolve(true)
        }

        return Promise.resolve(false)
    }

    private async handleDismiss(action: ModalAction): Promise<boolean> {
        await this.modalComponent?.notifyDismissed(action)

        if (action.callback) {
            // eslint-disable-next-line no-useless-call
            action.callback.call(action)
        }

        return Promise.resolve(true)
    }

    private buildResult(action: ModalAction): ModalResult {
        return <ModalResult>{
            type: action?.type,
            data: this.modalComponent?.getData() ?? null,
            tag: action?.tag ?? null,
        }
    }

    private shouldRequestDismissConfirmation(action: ModalAction): boolean {
        return (
            (action.type === ModalActionType.DISMISS && this.modal.shouldConfirmDismiss && this.modal.isDismissConfirmed !== true) ?? false
        )
    }
}
