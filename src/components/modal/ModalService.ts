import Vue from 'vue'
import { ModalActionType, ModalProps, ModalResult } from './Modal'

export default class ModalService extends Vue {
    private state: { modals: ModalProps[] } = Vue.observable({ modals: [] })

    get modals(): ModalProps[] {
        return this.state.modals
    }

    public open(modalProps: ModalProps) {
        this.state.modals.push(modalProps)
    }

    public async dismiss(
        modal: ModalProps,
        result: ModalResult | { type: ModalActionType; data: any } = { type: ModalActionType.DISMISS, data: null }
    ) {
        if (this.state.modals.includes(modal)) {
            switch (result.type) {
                case ModalActionType.DISMISS:
                    modal.onDismiss?.call(null)
                    break
                case ModalActionType.ACCEPT:
                    await modal.onAccept?.call(null, result)
                    break
            }
            // remove modal
            this.state.modals = this.state.modals.filter((value) => value !== modal)
        }
    }
}
