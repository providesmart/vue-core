import { NAME_MODAL_CONTAINER, NAME_MODAL_FLOATING, NAME_MODAL_SLIDE_IN } from '../../constants/components';
import ModalContainerComponent from './ModalContainer.vue';
import FloatingModalComponent from './FloatingModal.vue';
import SlideInModalComponent from './SlideInModal.vue';
import ModalService from './ModalService';

export const ModalModule: any = {
    install: (Vue: any, options: any) => {

        if (!options || !options.inject) {
            throw new Error('ModalModule expects an inject callback on install.');
        }

        options.inject('modalService', new ModalService(options.app));

        Vue.component(NAME_MODAL_CONTAINER, ModalContainerComponent);
        Vue.component(NAME_MODAL_FLOATING, FloatingModalComponent);
        Vue.component(NAME_MODAL_SLIDE_IN, SlideInModalComponent);
    }
}
