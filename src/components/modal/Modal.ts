import { Component } from 'vue'
import { FontawesomeObject, IconDefinition } from '@fortawesome/fontawesome-svg-core'
import ModalableComponent from './ModalableComponent'

export enum ModalType {
    FLOATING = 'floating-modal',
    SLIDE_IN = 'slide-in-modal'
}

export enum ModalActionType {
    DISMISS,
    ACCEPT,
}

/**
 * Used when modal receives a click.
 */
export interface ModalResult {
    type: ModalActionType
    data?: any
    tag?: string
}

export interface ModalAction {
    type: ModalActionType
    name?: string
    tag?: string
    variant?: 'secondary' | 'success' | 'danger' | 'link' | string | null
    icon?: IconDefinition,
    callback?: Function
}

export interface ModalProps {
    type: ModalType | Component
    variant?: 'secondary' | 'success' | 'danger' | string | null
    component?: Component<ModalableComponent>
    props?: object
    icon?: string[]
    hideIcon?: boolean
    title?: string
    message?: string
    fields?: any[]
    onAccept?: ((result: ModalResult | null) => void) | null
    onDismiss?: (() => void) | null
    actions?: ModalAction[]
    shouldConfirmDismiss?: boolean
    isDismissConfirmed?: boolean
}
