import { NAME_SKELETON, NAME_SKELETON_WRAPPER } from '../../constants/components'
import SkeletonWrapper from './SkeletonWrapper.vue'
import Skeleton from './Skeleton.vue'

export const SkeletonModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_SKELETON_WRAPPER, SkeletonWrapper)
        Vue.component(NAME_SKELETON, Skeleton)
    },
}
