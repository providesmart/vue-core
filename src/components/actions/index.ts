import ActionService from './ActionService'

export const ActionModule: any = {
    install: (Vue: any, options: any) => {
        options.inject('actionService', new ActionService(options.app))
    },
}
