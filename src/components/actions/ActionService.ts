import Vue from 'vue'
import { Item, Model } from '@vuex-orm/core'
import Resource from '@provide-smart/core/src/models/Resource'
import { ModalActionType, ModalType } from '@provide-smart/core/src/components/modal/Modal'
import ActionInterface from './ActionInterface'
import ActionForm from './ActionForm.vue'

export default class ActionService extends Vue {
    protected app!: Vue

    public constructor(app: any) {
        super(app)

        this.app = app
    }

    public run(resource: Resource, models: Item<Model>[] | 'all', action: ActionInterface, payload: Record<string, any> = {}, queryParams: Record<string, any> = {}) {
        const actionCallback = (payload: Record<string, any> = {}) => {
            return new Promise((resolve: any, reject: Function) => {
                this.$store
                    .$db()
                    .model(resource?.id)
                    .api()
                    .action(
                        action,
                        typeof models === 'string' ? models : models.map((model: any) => model.id),
                        payload,
                        queryParams,
                    )
                    .then((response: any) => {
                        this.handleActionResponse(response)
                        resolve(response)
                    })
                    .catch((error: any) => {
                        reject(error)
                    })
            })
        }

        if (!action.withoutConfirmation) {
            this.app.$modalService.open({
                title: action.name,
                message: action.confirmText,
                variant: action.destructive ? 'danger' : 'success',
                icon: action.icon,
                type: ModalType.FLOATING,
                component: action.fields && action.fields.length ? ActionForm : undefined,
                props: { action, payload },
                onAccept: (modalResult: any) => {
                    return actionCallback(modalResult.data)
                },
                actions: [
                    { type: ModalActionType.DISMISS, name: action.cancelButtonText, variant: 'default' },
                    { type: ModalActionType.ACCEPT, name: action.confirmButtonText, variant: action.destructive ? 'danger' : 'success' },
                ],
            })
        } else {
            actionCallback()
        }
    }

    protected handleActionResponse(response: any) {
        const data = response?.response?.data || {}

        switch (Object.keys(data)[0]) {

        case 'download':
            window.open(data.download, '_blank')
            break

        case 'message':
            this.app.$toasts.success({
                title: data.message,
                description: '',
            })
            break

        case 'danger':
            this.app.$toasts.error({
                title: data.danger,
                description: '',
            })
            break

        default:
            console.log('Unknown response', response)
            break
        }
    }
}
