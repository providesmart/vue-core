import Field from '../../models/fields/Field'

export default interface ActionInterface {
    availableForEntireResource?: boolean
    cancelButtonText?: string
    confirmButtonText?: string
    confirmText?: string
    class?: string
    component: string
    destructive: boolean
    fields?: Field[]
    icon?: string[]
    name: string
    showOnDetail?: boolean
    showOnIndex?: boolean
    showOnTableRow?: boolean
    standalone: boolean
    uriKey: string
    withoutConfirmation: boolean
}
