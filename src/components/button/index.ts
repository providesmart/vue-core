import { NAME_BUTTON } from '../../constants/components';
import Button from './Button.vue';

export const ButtonModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_BUTTON, Button);
    }
}
