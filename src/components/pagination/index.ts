import { NAME_PAGINATION } from '../../constants/components';
import Pagination from './Pagination.vue';

export const PaginationModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_PAGINATION, Pagination);
    }
}
