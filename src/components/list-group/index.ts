import ListGroup from './ListGroup.vue';
import ListGroupItem from './ListGroupItem.vue';
import { NAME_LIST_GROUP, NAME_LIST_GROUP_ITEM } from '../../constants/components';

export const ListGroupModule: any = {
    install: (Vue: any) => {

        Vue.component(NAME_LIST_GROUP, ListGroup);
        Vue.component(NAME_LIST_GROUP_ITEM, ListGroupItem);

    }
}
