import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';
import Query from '~/modules/@provide-smart/core/src/utils/Query';
import FilterInterface from '~/modules/@provide-smart/core/src/components/datatable/filter/interface';

@Component
export default class ResourceFilterMixin extends Vue {

    @Prop({
        type: Object, default: () => {
        }
    })
    query!: Query

    @Prop({type: Object})
    service: any

    public get filters(): FilterInterface[] {
        return []
    }

    public get stickyFilters() {
        return this.filters.filter((filter: FilterInterface) => filter.sticky)
    }

    public filterValue(name: string) {
        return this.query?.filters?.[name]
    }

    public filterValueForDisplay(name: string) {
        const filter: any = this.filterValue(name)

        if (typeof filter === 'object') {
            const filterValue: any = Object.values(filter)

            if (typeof filterValue !== 'object') {
                return filterValue
            }

            return filterValue.join(', ')
        }

        return filter
    }

    public filterIsActive(name: string) {
        const filter: any = this.query?.filter?.[name]
        return !!filter
    }

    public getFilterAttribute(filter: FilterInterface) {
        return filter.attribute
    }

    public isTextFilter(filter: FilterInterface) {
        return filter.component === 'text-filter'
    }

    public isBooleanFilter(filter: FilterInterface) {
        return filter.component === 'boolean-filter'
    }

    public isSelectFilter(filter: FilterInterface) {
        return filter.component === 'select-filter'
    }

    public isRangeFilter(filter: FilterInterface) {
        return filter.component === 'range-filter'
    }

    public isDateRangeFilter(filter: FilterInterface) {
        return filter.component === 'date-range-filter'
    }

    public bindingsForFilter(filter: FilterInterface) {
        let bindings: any = {
            useInDropdown: true,
            query: this.query,
            label: filter.name,
            filterName: this.getFilterAttribute(filter),
            showLabel: false
        }

        switch (filter.component) {
        case 'select-filter':
            bindings = {
                ...bindings,
                options: filter.options,
                labelKey: filter.api ? this.getOptionLabelKey(filter, '_title') : this.getOptionLabelKey(filter, 'label'),
                valueKey: filter.api ? this.getOptionLabelKey(filter, 'id') : this.getOptionValueKey(filter, 'value'),
                multiple: true,
                api: filter.api,
                entity: filter.relatedResourceName
            }
            break

        case 'boolean-filter':
            bindings = {
                ...bindings,
                showLabel: true
            }
            break

        case 'range-filter':
            bindings = {
                ...bindings,
                fromLabel: 'From',
                toLabel: 'To',
            }
            break
        default:
            break
        }

        return bindings
    }

    protected getOptionLabelKey(filter: FilterInterface, defaultValue: string) {
        return filter.optionLabelKey || defaultValue
    }

    protected getOptionValueKey(filter: FilterInterface, defaultValue: string) {
        return filter.optionValueKey || defaultValue
    }


}
