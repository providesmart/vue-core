import { NAME_TAB, NAME_TABS } from '../../constants/components';
import Tabs from './Tabs.vue';
import Tab from './Tab.vue';

export const TabsModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_TABS, Tabs);
        Vue.component(NAME_TAB, Tab);
    }
}
