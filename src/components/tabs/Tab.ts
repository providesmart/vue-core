export interface Tab {
    name: string
    key: string
    selected: boolean
    action: () => {}
}
