import PageHeader from './PageHeader.vue';
import { NAME_HEADING_PAGE } from '../../constants/components';

export const HeaderModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_HEADING_PAGE, PageHeader);
    }
}
