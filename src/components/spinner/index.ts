import Spinner from './Spinner.vue'

export const SpinnerModule: any = {
    install: (Vue: any) => {
        Vue.component('PsSpinner', Spinner)
    },
}
