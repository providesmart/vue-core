import DropdownSelectInput from '@provide-smart/core/src/components/form/inputs/DropdownSelectInput.vue'
import SelectField from '@provide-smart/core/src/components/form/fields/detail/SelectField.vue'
import FileField from '@provide-smart/core/src/components/form/fields/detail/FileField.vue'
import CurrencyField from '@provide-smart/core/src/components/form/fields/detail/CurrencyField.vue'
import CurrencyInput from '@provide-smart/core/src/components/form/inputs/CurrencyInput.vue'
import BelongsToManyField from '@provide-smart/core/src/components/form/fields/detail/BelongsToManyField.vue'
import { NAME_FORM_CHECKBOX } from '../../constants/components'
import Checkbox from './checkbox/Checkbox.vue'
import TextField from './fields/detail/TextField.vue'
import DefaultField from './fields/detail/DefaultField.vue'
import TextInput from './inputs/TextInput.vue'
import PasswordInput from './inputs/PasswordInput.vue'
import BelongsToField from './fields/detail/BelongsToField.vue'
import TextareaInput from './inputs/TextareaInput.vue'
import TextareaField from './fields/detail/TextareaField.vue'
import GeneratedCodeField from './fields/detail/GeneratedCodeField.vue'
import BooleanField from './fields/detail/BooleanField.vue'

import FormTextField from './fields/form/TextField.vue'
import FormDefaultField from './fields/form/DefaultField.vue'
import FormBelongsToField from './fields/form/BelongsToField.vue'
import FormSelectField from './fields/form/SelectField.vue'
import FormTextareaField from './fields/form/TextareaField.vue'
import FormPasswordField from './fields/form/PasswordField.vue'
import FormBooleanField from './fields/form/BooleanField.vue'
import FormGeneratedCodeField from './fields/form/GeneratedCodeField.vue'
import FormDateTimeField from './fields/form/DateTimeField.vue'
import DateInput from './inputs/DateInput.vue'
import FormDateField from './fields/form/DateField.vue'
import FormCurrencyField from './fields/form/CurrencyField.vue'

export const FormModule: any = {
    install: (Vue: any) => {
        Vue.component(NAME_FORM_CHECKBOX, Checkbox)

        /**
         * INPUTS
         */
        Vue.component('TextInput', TextInput)
        Vue.component('DateInput', DateInput)
        Vue.component('CurrencyInput', CurrencyInput)
        Vue.component('PasswordInput', PasswordInput)
        Vue.component('TextareaInput', TextareaInput)
        Vue.component('DropdownSelectInput', DropdownSelectInput)

        /**
         * FIELDS
         */

        // DETAIl
        Vue.component('DetailDefaultField', DefaultField)
        Vue.component('DetailTextField', TextField)
        Vue.component('DetailCurrencyField', CurrencyField)
        Vue.component('DetailSelectField', SelectField)
        Vue.component('DetailTextareaField', TextareaField)
        Vue.component('DetailBelongsToField', BelongsToField)
        Vue.component('DetailBelongsToManyField', BelongsToManyField)
        Vue.component('DetailNovaAttachMany', BelongsToManyField)
        Vue.component('DetailGeneratedCodeField', GeneratedCodeField)
        Vue.component('DetailFileField', FileField)
        Vue.component('DetailBooleanField', BooleanField)

        // FORM
        Vue.component('FormDefaultField', FormDefaultField)
        Vue.component('FormTextField', FormTextField)
        Vue.component('FormTextareaField', FormTextareaField)
        Vue.component('FormDateTime', FormDateTimeField)
        Vue.component('FormDate', FormDateField)
        Vue.component('FormCurrencyField', FormCurrencyField)
        Vue.component('FormPasswordField', FormPasswordField)
        Vue.component('FormBooleanField', FormBooleanField)
        Vue.component('FormBelongsToField', FormBelongsToField)
        Vue.component('FormSelectField', FormSelectField)
        Vue.component('FormGeneratedCodeField', FormGeneratedCodeField)
    },
}
