export enum FeedFilterType {
    FILTER = 'filter',
    ORDER_BY = 'orderBy'
}

export interface FeedFilterOption {
    value: string | number | null,
    label: string | number,
    icon?: object
}

export interface FeedFilter {
    type: FeedFilterType,
    attribute: string,
    options: FeedFilterOption[]
}

export type FeedFilters = FeedFilter[]
