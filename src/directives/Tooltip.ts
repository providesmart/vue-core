import { DirectiveOptions } from 'vue'

class Tooltip implements DirectiveOptions {
    public bind(el: HTMLElement, bindings: any, vnode: any) {
        console.log('BIND ELEMENT DIRECTIVE', el, bindings, vnode)
    }
}

export default new Tooltip()
