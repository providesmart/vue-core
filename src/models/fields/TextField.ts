import Field from './Field'

export default class TextField extends Field {
    type: string = 'text'

    constructor(props: TextField | Field) {
        super(props)

        Object.assign(this, props)
    }
}
