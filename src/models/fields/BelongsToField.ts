import Field from './Field'

export default class BelongsToField extends Field {
    belongsToId!: number | null
    belongsToRelationship!: string
    debounce!: number
    displaysWithTrashed: boolean = true
    label!: string
    resourceName!: string
    reverse!: boolean
    searchable: boolean = false
    withSubtitles: boolean = false
    showCreateRelationButton: boolean = false
    singularLabel!: string
    viewable: boolean = true

    constructor(props: BelongsToField | Field) {
        super(props)

        Object.assign(this, props)
    }
}
