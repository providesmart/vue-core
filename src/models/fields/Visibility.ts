export default class Visibility {
    hideByDefault: Boolean  = false
    showOnIndex: Boolean    = true
    showOnDetail: Boolean   = true
    showOnCreation: Boolean = true
    showOnUpdate: Boolean   = true

    constructor(props: object) {
        Object.assign(this, props)
    }
}
