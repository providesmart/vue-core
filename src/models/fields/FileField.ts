import Field from '@provide-smart/core/src/models/fields/Field'

export class FileField extends Field {
    public acceptedTypes!: string
}
