import Field from './Field'

export default class GeneratedCodeField extends Field {
    generatedValue!: string
}
