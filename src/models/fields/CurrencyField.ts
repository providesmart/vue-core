import Field from './Field'
import NumberField from './NumberField'

export default class CurrencyField extends NumberField {
    currency: string = '€'
    currency_name: string = 'Euro'

    constructor(props: CurrencyField | NumberField | Field) {
        super(props)

        Object.assign(this, props)
    }
}
