import Field from './Field'

export default class NumberField extends Field {
    type: string = 'number'
    step!: number | null

    constructor(props: NumberField | Field) {
        super(props)

        Object.assign(this, props)
    }
}
