import Visibility from './Visibility'
import { ClipMode } from '~/modules/@provide-smart/core/src/components/datatable/columns/enum'

export default class Field {
    attribute!: string
    component!: string | object
    helpText: string | null       = null
    indexName!: string
    name!: string
    nullable: boolean             = false
    type!: string
    panel: string | null          = null
    prefixComponent!: boolean
    readonly: boolean             = false
    required: boolean             = false
    sortable: boolean             = false
    searchable: boolean           = false
    sortableUriKey!: string
    stacked: boolean              = false
    textAlign: string             = 'left'
    foreignKey!: string
    validationKey!: string
    value: any                    = null
    id!: number | string
    resource!: string
    resourceName!: string
    isRelation: boolean           = false
    isComputed: boolean           = false
    isQuickCreate: boolean        = false
    showInHeading: boolean        = false
    width: string | number | null = null
    clipMode: ClipMode            = ClipMode.CLIP
    options!: any
    lines!: any
    min!: number
    max!: number
    visibility: Visibility        = new Visibility({
        showOnIndex: true,
        showOnDetail: true,
        showOnCreation: true,
        showOnUpdate: true,
    })


    constructor(props: Field | any) {
        Object.assign(this, props)

        if (Object.prototype.hasOwnProperty.call(props, 'visibility')) {
            Object.assign(this, {
                visibility: new Visibility(props.visibility),
            })
        }
    }

    public isFileField() {
        return this.component === 'file-field'
    }
}
