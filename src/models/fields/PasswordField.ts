import Field from './Field'

export default class PasswordField extends Field {
    type: string = 'password'

    constructor(props: PasswordField | Field) {
        super(props)

        Object.assign(this, props)
    }
}
