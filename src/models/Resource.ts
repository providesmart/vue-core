import { v4 as uuidv4 } from 'uuid'
import Model from './Model'
import Field from './fields/Field'

export default class Resource extends Model {
    static entity = 'resources'
    static apiConfig = Object.assign({}, Model.apiConfig, {
        baseURL: process.env.NUXT_ENV_API_BASE_URL + `${Resource.entity}`,
    })

    public fields!: Field[]
    public id!: string
    public actions!: any[]
    public filters!: any[]
    public lenses!: any[]
    public specs!: any[]

    get panels() {
        return this.$getAttributes().fields.reduce((carry: any[], field: Field) => {
            if (field.panel && !carry.find((panel: string | null) => panel === field.panel)) {
                carry.push(field.panel)
            }

            return carry
        }, [])
    }

    get badges() {
        return this.fields.filter((field: Field) => field.showInHeading && field.component === 'badge-field')
    }

    static fields() {
        return {
            uuid: this.uid(() => uuidv4()),
            id: this.string(null).nullable(),
            fields: this.attr(null).nullable(),
            actions: this.attr(null).nullable(),
            filters: this.attr(null).nullable(),
            lenses: this.attr(null).nullable(),
            specs: this.attr([]),
        }
    }
}
