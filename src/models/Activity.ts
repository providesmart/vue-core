// @ts-nocheck
import { v4 as uuidv4 } from 'uuid'
import Model from '@provide-smart/core/src/models/Model'
import User from '@provide-smart/core/src/models/User'

export default class Activity extends Model {
    static entity    = 'activities'
    static apiConfig = Object.assign({}, Model.apiConfig, {
        baseURL: process.env.NUXT_ENV_API_BASE_URL + Activity.entity,
    })

    public id: number
    // eslint-disable-next-line camelcase
    public log_name: string
    public description: string
    // eslint-disable-next-line camelcase
    public subject_type: string
    // eslint-disable-next-line camelcase
    public subject_id: number
    // eslint-disable-next-line camelcase
    public causer_type: string
    // eslint-disable-next-line camelcase
    public causer_id: number
    public properties: any
    // eslint-disable-next-line camelcase
    public created_at: string
    // eslint-disable-next-line camelcase
    public updated_at: string
    public _title: string
    public causer: any
    public subject: any
    // eslint-disable-next-line camelcase
    public causer_user: User | null

    static fields() {
        return {
            uuid: this.uid(() => uuidv4()),
            id: this.number(null).nullable(),
            log_name: this.string(null).nullable(),
            description: this.string(null).nullable(),
            subject_type: this.string(null).nullable(),
            subject_id: this.number(null).nullable(),
            causer_type: this.string(null).nullable(),
            causer_id: this.attr(null).nullable(),
            properties: this.attr(null).nullable(),
            created_at: this.attr(null).nullable(),
            updated_at: this.attr(null).nullable(),
            _title: this.attr(null).nullable(),

            causer: this.morphTo('causer_id', 'causer_type'),
            subject: this.morphTo('subject_id', 'subject_type'),
            causer_user: this.belongsTo(User, 'causer_id', 'id'),
        }
    }
}
