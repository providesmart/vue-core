import { Model as BaseModel } from '@vuex-orm/core'
import { Config } from '@vuex-orm/plugin-axios'
import qs from 'qs'
import { EventBus } from '@provide-smart/core/src/utils/EventBus'
import QueryResolver from '@provide-smart/core/src/utils/QueryResolver'
import UrlQueryParams from '../utils/UrlQueryParams'
import ActionInterface from '~/modules/@provide-smart/core/src/components/actions/ActionInterface'
import Query from '~/modules/@provide-smart/core/src/utils/Query'

export default class Model extends BaseModel {
    static apiConfig: Config = {
        dataKey: 'data',
        actions: {
            index(urlParams: UrlQueryParams = { page: 1 }, autoEagerLoad = false, config = {}) {
                const params = !autoEagerLoad ? urlParams : { ...urlParams, autoEagerLoad: true }

                return this.get(null, {
                    params,
                    paramsSerializer: (params: any) => {
                        return qs.stringify(params)
                    },
                    ...config,
                })
            },
            show(id: number | string, config = {}) {
                return this.get(id + '', config)
            },
            create(data: [], config = {}) {
                const request = this.post('', data, config)

                EventBus.$emit('model.create.' + this.model.entity, {
                    data,
                    request,
                })

                return request
            },
            update(id: number | string, data: Record<string, any> | FormData, config = {}, patch = false) {
                if (data instanceof FormData) {
                    data.append('_method', patch ? 'PATCH' : 'PUT')
                } else {
                    data._method = patch ? 'PATCH' : 'PUT'
                }

                const request = patch
                    ? this.patch('' + id, data, config)
                    : this.post('' + id, data, config)

                EventBus.$emit('model.update.' + this.model.entity, {
                    id,
                    data,
                    request,
                })

                return request
            },
            destroy(id: number | string, config = {}) {
                const request = this.delete('' + id, config)

                EventBus.$emit('model.destroy.' + this.model.entity, {
                    id,
                    request,
                })

                return request
            },
            action(action: ActionInterface, ids: number[] | string[] | 'all', payload: Record<string, any> = {}, queryParams: Record<string, any> = {}) {
                const config = {}

                const request = this.post(
                    'actions?action=' + action.uriKey,
                    {
                        resources: typeof ids !== 'string' ? ids.join(',') : ids,
                        ...payload,
                    },
                    {
                        ...config,
                        params: queryParams,
                        paramsSerializer: (params: any) => {
                            return qs.stringify(params)
                        },
                    },
                )

                if (action.destructive) {
                    request.then(() => {
                        this.model.delete((model: Model) => {
                            return ids.includes(model.id as never)
                        })
                    })
                }

                return request
            },
        },
    }

    public static entity: string
    public static translationKey: string
    public id!: string
    public _resourceName!: string

    public get _resourceSlug() {
        return this._resourceName + '.' + this.id
    }

    static findRelationField(relationName: string) {
        const fields = this.fields()

        return Object.keys(fields).reduce((carry: any, fieldName: any) => {
            const field: any = fields[fieldName]

            if (field.parent && field?.parent?.entity === relationName) {
                carry = fieldName
            }

            return carry
        }, null)
    }

    static applyQuery(query: Query) {
        return new QueryResolver(this.query(), query).getQuery()
    }
}
