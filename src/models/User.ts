import { v4 as uuidv4 } from 'uuid'
import Model from '@provide-smart/core/src/models/Model'

export default class User extends Model {
    static entity = 'users'

    static apiConfig = Object.assign({}, Model.apiConfig, {
        baseURL: `/backend/api/${User.entity}`,
    })

    // eslint-disable-next-line camelcase
    public first_name!: string
    // eslint-disable-next-line camelcase
    public last_name!: string

    public get name() {
        return this.first_name + ' ' + this.last_name
    }

    public set name(avalue) {
        // nothing to do
    }

    static fields() {
        return {
            uuid: this.uid(() => uuidv4()),
            id: this.number(null).nullable(),
            name: this.string(null).nullable(),
            first_name: this.string(null).nullable(),
            last_name: this.string(null).nullable(),
            email: this.string(null).nullable(),
            // email_verified_at: this.string(null).nullable(),
            // created_at: this.attr(null).nullable(),
            // updated_at: this.attr(null).nullable(),
            _title: this.attr(null).nullable(),
            _subtitle: this.attr(null).nullable(),

            // roles: this.morphToMany(Role, ModelHasRole, 'role_id', 'model_id', 'model_type'),
            // permissions: this.morphToMany(Permission, ModelHasPermission, 'permission_id', 'model_id', 'model_type'),
        }
    }
}
