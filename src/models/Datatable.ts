// @ts-ignore
import { v4 as v4uuid } from 'uuid'
import Model from '../models/Model'
import Query from '../utils/Query'

export default class Datatable extends Model {
    static entity = 'datatables'

    static primaryKey = 'uuid'
    uuid!: string
    name!: string
    model!: string
    query!: Query
    selection!: Record<string, any>
    selectAll!: boolean
    reload!: boolean
    total!: number
    pages!: number
    resultingIds!: string[] | number[]

    static fields() {
        return {
            uuid: this.uid(() => v4uuid()),
            name: this.attr(null).nullable(),
            model: this.attr(''),
            query: this.attr({
                page: 1,
                perPage: 10,
            } as Query),
            resultingIds: this.attr([]),
            selection: this.attr({}),
            selectAll: this.boolean(false),
            reload: this.boolean(false),
            total: this.number(null).nullable(),
            pages: this.number(null).nullable(),
        }
    }

    public $reload() {
        this.$update({
            reload: true,
            selection: {},
        })
    }
}
