import { Plugin } from '@nuxt/types'

export interface ToastSettings {
    type?: string
    title?: string
    description?: string
    duration?: number
    showCloseButton?: boolean
    randomId?: number
    component?: any
    componentSettings?: object
}

interface ToastsService {
    open: (settings: ToastSettings) => void
    primary: (settings: ToastSettings) => void
    secondary: (settings: ToastSettings) => void
    error: (settings: ToastSettings) => void
    danger: (settings: ToastSettings) => void
    success: (settings: ToastSettings) => void
    warning: (settings: ToastSettings) => void
    info: (settings: ToastSettings) => void
    close: (index: number) => void
    closeAll: () => void
}

declare module 'vue/types/vue' {
    // this.$myInjectedFunction inside Vue components
    interface Vue {
        $toasts: ToastsService
    }
}

declare module '@nuxt/types' {
    // nuxtContext.app.$myInjectedFunction inside asyncData, fetch, plugins, middleware, nuxtServerInit
    interface NuxtAppOptions {
        $toasts: ToastsService
    }

    // nuxtContext.$myInjectedFunction
    interface Context {
        $toasts: ToastsService
    }
}

const plugin: Plugin = ({ store }, inject) => {
    if (!store) throw new Error('Vuex is required. Enable by setting store: true in nuxt.config.js')

    // Register store module
    store.registerModule('toasts', {
        namespaced: true,
        state: {
            toasts: [],
        } as {
            toasts: ToastSettings[]
        },

        mutations: {
            open(state, settings: ToastSettings) {
                state.toasts.push({
                    type: 'error',
                    title: '???',
                    duration: 5,
                    description: '????????',
                    showCloseButton: true,
                    component: null,
                    randomId: 1 * Math.random(),
                    ...settings,
                })
            },

            close(state, payload) {
                state.toasts.splice(payload, 1)
            },

            tick(state) {
                state.toasts = state.toasts
                    .slice(0, 3)
                    .map((toast: ToastSettings) => ({ ...toast, duration: (toast.duration ?? 0) - 1 }))
                    .filter((toast: ToastSettings) => Number(toast.duration) >= 0)
            },

            closeAll(state) {
                state.toasts = []
            },

            changeSettings(state, payload: any) {
                if (payload.toast) {
                    state.toasts.splice(state.toasts.indexOf(payload.toast), 1, { ...payload.toast, ...payload.settings })
                } else {
                    state.toasts.splice(state.toasts.length - 1, 1, { ...state.toasts.slice(-1)[0], ...payload })
                }
            },
        },

        getters: {
            visible(state): ToastSettings[] {
                return state.toasts.slice(0, 3)
            },
            first(state): ToastSettings {
                return state.toasts.slice(-1)[0]
            },
        },
    })

    // Make function available
    inject('toasts', {
        open: (settings: ToastSettings) => store.commit('toasts/open', settings),
        primary: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'primary' }),
        secondary: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'secondary' }),
        error: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'danger' }),
        danger: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'danger' }),
        success: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'success' }),
        warning: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'warning' }),
        info: (settings: ToastSettings) => store.commit('toasts/open', { ...settings, type: 'info' }),
        tick: () => store.commit('toasts/tick'),
        close: () => store.commit('toasts/close'),
        closeAll: () => store.commit('toasts/closeAll'),
    })
}

export default plugin
