import { Context, Plugin } from '@nuxt/types';
import BaseResource from '~/modules/@provide-smart/core/src/utils/resources/BaseResource';

declare module 'vue/types/vue' {
    // this.$myInjectedFunction inside Vue components
    interface Vue {
        $resourceService: ResourceService
    }
}

export class ResourceService {
    protected resources: Record<string, any> = {}

    public constructor(protected context: Context) {
    }

    public register(name: string, resourceClass: typeof BaseResource) {
        this.resources[name] = resourceClass
    }

    public get(name: string) {
        return this.resources[name] ? new this.resources[name](this.context) : false
    }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const plugin: Plugin = (context: Context, inject) => {
    inject('resourceService', new ResourceService(context))
}

export default plugin
