import Vue from 'vue'
import { Component, Prop } from 'nuxt-property-decorator'
import { Item } from '@vuex-orm/core'
import Resource from '@provide-smart/core/src/models/Resource'
import Model from '@provide-smart/core/src/models/Model'

@Component
export default class AcceptsEntity<M extends typeof Model = typeof Model> extends Vue {
    @Prop({ type: String })
    entity!: string

    @Prop({ type: [Number, String] })
    entityId!: number | string

    @Prop({ type: String })
    viaResource!: string

    @Prop({ type: [Number, String] })
    viaResourceId!: number | string

    @Prop({ type: Boolean, default: true })
    prefetch!: boolean

    public initialized = false

    get entityModel(): M {
        return this.$store.$db().model(this.entity) as M
    }

    get entityInstance(): Item<any> | Record<string, any> {
        return (
            this.entityModel
                .query()
                .withAll()
                .whereId(parseInt(this.entityId + ''))
                .first() || {}
        )
    }

    get resource(): Item<any> | Record<string, any> {
        return this.$resourceService.get(this.entity) || Resource.find(this.entityModel.entity) || {}
    }

    async initialize() {
        if (!this.$resourceService.get(this.entity) && !this.resource?.id) {
            await Resource.api().show(this.entityModel.entity)
        }

        if (this.prefetch && this.entityId) {
            await this.fetchEntityInstance()
        }

        this.initialized = true
    }

    protected async fetchEntityInstance() {
        const instance = await this.entityModel.api().show(this.entityId)

        this.$emit('entityLoaded', this.entityInstance)

        return instance
    }
}
