export default function(model: any) {
    return {
        created_at: model.string(null).nullable(),
        updated_at: model.string(null).nullable(),
    }
}
