import { Query as VuexQuery } from '@vuex-orm/core'
import Query, { QueryFilterEnum, QueryFilters } from '@provide-smart/core/src/utils/Query'

export default class QueryResolver {
    constructor(protected query: VuexQuery, protected psQuery: Query) {
        this.resolveFilters()
        this.resolveOrderBy()
    }

    public getQuery() {
        return this.query
    }

    public resolveFilters() {
        Object.keys(this.psQuery?.filters || {}).forEach((filterKey: string) => {
            const filterValue: QueryFilters | any = this.psQuery?.filters?.[filterKey]
            const relationKey                     = null

            if (!filterValue) {
                return
            }

            if (typeof filterValue === 'string') {
                this.where(filterKey, filterValue)
            } else {
                Object.keys(filterValue || {}).forEach((operator: any) => {
                    this.resolveOperatorQuery(operator, filterKey, filterValue[operator])
                })
            }
        })
    }

    public resolveOrderBy() {
        Object.keys(this.psQuery?.orderBy || {}).forEach((orderKey: string) => {
            this.query.orderBy(orderKey, this.psQuery.orderBy?.[orderKey] || 'asc')
        })
    }

    protected resolveOperatorQuery(operator: QueryFilterEnum, key: string, value: string | any[]) {
        switch (operator) {
        case QueryFilterEnum.EQUAL:
            this.where(key, value)
            break
        case QueryFilterEnum.NOT_EQUAL:
            this.where(key, (v: any) => v !== value)
            break
        case QueryFilterEnum.GREATHER_THAN:
            this.where(key, (v: any) => v > value)
            break
        case QueryFilterEnum.GREATHER_THAN_OR_EQUAL:
            this.where(key, (v: any) => v >= value)
            break
        case QueryFilterEnum.LESS_THAN:
            this.where(key, (v: any) => v < value)
            break
        case QueryFilterEnum.LESS_THAN_OR_EQUAL:
            this.where(key, (v: any) => v <= value)
            break
        case QueryFilterEnum.LIKE:
            this.where(key, (v: any) => v.includes(value))
            break
        case QueryFilterEnum.IN:
            this.where(key, (v: any) => v.contains(value))
            break
        }
    }

    protected where(key: string, value: any) {
        if (key.includes('.')) {
            const splittedKey = key.split('.')
            this.query.whereHas(splittedKey[0], (query: VuexQuery) => {
                query.where(splittedKey[1], value)
            })
        } else {
            this.query.where(key, value)
        }
    }
}
