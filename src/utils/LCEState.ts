export enum LCEState {
    Loading,
    Content,
    Empty,
    Error,
}
