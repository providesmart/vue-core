export default function(model: any) {
    return {
        _title: model.string('').nullable(),
        _subtitle: model.string('').nullable(),
        _specs: model.attr([]),
        _resourceName: model.attr(model.entity),
    }
}
