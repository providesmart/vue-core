import Query from './Query'

export default interface UrlQueryParams extends Query {
    with?: string[] | string
    autoEagerLoad?: boolean
}
