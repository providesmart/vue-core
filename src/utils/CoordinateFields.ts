export default function (model: any) {
    return {
        latitude: model.attr(null).nullable(),
        longitude: model.attr(null).nullable(),
    }
}
