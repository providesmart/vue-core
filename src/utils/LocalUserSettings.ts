export default class LocalUserSettings {

    protected static settingPrefix = 'settings:'

    public static save(name: string, settings: Record<string, any>) {
        localStorage.setItem(this.settingPrefix + name, JSON.stringify(settings))
    }

    public static get(name: string) {
        const settings: string | null = localStorage.getItem(this.settingPrefix + name)

        return settings ? JSON.parse(settings) : false
    }

}
