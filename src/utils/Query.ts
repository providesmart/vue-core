export enum QueryFilterEnum {
    EQUAL                  = 'eq',
    NOT_EQUAL              = 'neq',
    GREATHER_THAN          = 'gt',
    GREATHER_THAN_OR_EQUAL = 'gte',
    LESS_THAN              = 'lt',
    LESS_THAN_OR_EQUAL     = 'lte',
    LIKE                   = 'like',
    IN                     = 'in',
}

export interface QueryFilters {
    [field: string]: string | null | undefined | { [key in QueryFilterEnum]?: string | number | any[] }
}

export interface QueryFilter {
    [field: string]: string | null
}

export enum QueryOrderByEnum {
    ASC  = 'asc',
    DESC = 'desc',
}

export interface QueryOrderBy {
    [field: string]: QueryOrderByEnum
}

export default interface Query {
    page?: number
    perPage?: number
    search?: string
    filters?: QueryFilters
    filter?:QueryFilter
    filterJoin?: 'OR' | 'AND'
    orderBy?: QueryOrderBy,
    groupBy?: string,
    with?: string[] | string
    include?: string[] | string
}
