import { Context } from '@nuxt/types';

export default class BaseResource {
    // eslint-disable-next-line no-useless-constructor
    constructor(protected context: Context) {
    }

    protected t(translationString: string): string {
        return this.context.i18n.t(translationString) as string
    }
}
