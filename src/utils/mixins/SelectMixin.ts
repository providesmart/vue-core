import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';

@Component
export default class SelectMixin extends Vue {
    @Prop({type: Array})
    options!: any[]

    @Prop({type: Boolean, default: false})
    multiple!: boolean

    @Prop({type: String, default: 'value'})
    valueKey!: string

    @Prop({type: String, default: 'text'})
    labelKey!: string

    @Prop({type: Boolean, default: true})
    search!: boolean

    @Prop({type: Boolean, default: false})
    api!: boolean

    @Prop({type: Function, default: null})
    fetchOptions!: Function

    public visibleOptions: any[] = []
}
