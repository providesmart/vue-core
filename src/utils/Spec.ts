export default interface Spec {
    attribute: string
    icon: Object
    value?: string
}
