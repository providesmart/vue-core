import Vue from 'vue'
import Model from '@provide-smart/core/src/models/Model'

export default class ResourceHelper extends Vue {

    public translationString(entityName: string, plural = true) {
        const ResourceModel: typeof Model = this.$store.$db().model(entityName) as typeof Model

        return ResourceModel.translationKey + '_' + (plural ? 'PLURAL' : 'SINGULAR')
    }
}
