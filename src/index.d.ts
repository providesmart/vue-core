import ModalService from './components/modal/ModalService'
import ActionService from './components/actions/ActionService'
import ResourceHelper from '~/modules/@provide-smart/core/src/utils/ResourceHelper'
import '@nuxtjs/i18n/types/index'

declare module 'vue/types/vue' {
    interface Vue {
        readonly $modalService: ModalService
        readonly $actionService: ActionService
        readonly $eventBus: Vue
        readonly $resourceHelper: ResourceHelper
    }
}
